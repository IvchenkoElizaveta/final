function openTab(tabName) {
  var i;
  var x = document.getElementsByClassName("container");
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";  
  }
  document.getElementById(tabName).style.display = "block";  
}

var modal = document.querySelector(".banner-container");
var close = document.querySelector(".close");

close.onclick = function(event) {
    modal.style.display = "none";
}
